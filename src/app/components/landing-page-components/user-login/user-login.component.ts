import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { TrainerLoginService } from 'src/app/services/trainer-login.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css'],
})
export class UserLoginComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly trainerLoginService: TrainerLoginService ) {}
  ngOnInit(): void {}

  /**
   * Log in the trainer.
   * Store Trainer and All pokemons in Local Storage.
   * Create a New trainer if not already exists.
   */
  public onLoginClick(loginForm: NgForm): void {
    // Get username from form value
    const username = loginForm.value.username
    if (username.trim() === "") {
      throw new Error("That's not a username, Hackerboy!")
    }

    // Store trainer in LocalStorage and API and reroute to Catalogue
    this.trainerLoginService.logInTrainer(username);
  }
}
