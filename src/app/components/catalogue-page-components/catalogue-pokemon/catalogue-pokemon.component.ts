import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { TRAINER_STORAGE_KEY } from 'src/app/models/storage.model';
import { TrainerStorageService } from 'src/app/services/trainer-storage.service';
import { environment } from 'src/environments/environment';
import {Trainer, TrainerResponse} from "../../../models/trainer.model";
import {TrainerApiService} from "../../../services/trainer-api.service";
import capitalize from 'src/app/util/capitalize';

const { imgURL } = environment;

@Component({
  selector: 'app-catalogue-pokemon',
  templateUrl: './catalogue-pokemon.component.html',
  styleUrls: ['./catalogue-pokemon.component.css'],
})
export class CataloguePokemonComponent implements OnInit {
  public readonly url = imgURL;
  public isCaught = false;
  @Input() pokemon: Pokemon = {name: "", id: -1};

  public capitalize = (_input: string) => capitalize(_input);

  constructor(private readonly trainerStorageService: TrainerStorageService, private readonly trainerAPIService: TrainerApiService) {}

  ngOnInit(): void {
    const trainerData = localStorage.getItem(TRAINER_STORAGE_KEY);
    if (trainerData)
    {
      const trainer = JSON.parse(trainerData) as Trainer;
      if (trainer.pokemon.find(_pokemon => _pokemon.name === this.pokemon?.name) !== undefined)
      {
        this.isCaught = true;
      }
    }
  }

  public onClickButton = () =>
  {
    const trainerData = localStorage.getItem(TRAINER_STORAGE_KEY);
    if (trainerData)
    {
      const trainerResponse = JSON.parse(trainerData) as TrainerResponse;
  
      if (this.isCaught)
      {
        trainerResponse.pokemon = trainerResponse.pokemon.filter(_pokemon => _pokemon.name !== this.pokemon?.name);
      }
      else
      {
          if (this.pokemon)
          {
            const pokemon: Pokemon = {...this.pokemon};
            trainerResponse.pokemon.push(pokemon);
          }
      }
      this.isCaught = !this.isCaught;
      this.trainerStorageService.setTrainer(trainerResponse);
      const trainer = {username: trainerResponse.username, pokemon: trainerResponse.pokemon};
      this.trainerAPIService.updateTrainer(trainerResponse.id, trainer).subscribe();
    }
  }

}
