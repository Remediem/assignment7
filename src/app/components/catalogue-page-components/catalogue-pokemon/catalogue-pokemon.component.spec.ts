import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CataloguePokemonComponent } from './catalogue-pokemon.component';

describe('CataloguePokemonComponent', () => {
  let component: CataloguePokemonComponent;
  let fixture: ComponentFixture<CataloguePokemonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CataloguePokemonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CataloguePokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
