import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CataloguePokemonListComponent } from './catalogue-pokemon-list.component';

describe('CataloguePokemonListComponent', () => {
  let component: CataloguePokemonListComponent;
  let fixture: ComponentFixture<CataloguePokemonListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CataloguePokemonListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CataloguePokemonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
