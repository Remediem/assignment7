import {Component, HostListener, Input, OnInit} from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonApiService } from 'src/app/services/pokemon-api.service';
import { PokemonStorageService } from 'src/app/services/pokemon-storage.service';
import { environment } from 'src/environments/environment';
import convertPokemonFormat from "../../../util/convertPokemonFormat";

const { imgURL } = environment;

@Component({
  selector: 'app-catalogue-pokemon-list',
  templateUrl: './catalogue-pokemon-list.component.html',
  styleUrls: ['./catalogue-pokemon-list.component.css'],
})
export class CataloguePokemonListComponent implements OnInit {
  public url = imgURL;
  private loadingContent = false;

  get pokemonList(): Pokemon[] {
    return this.pokemonStorageService.getPokemon();
  }

  constructor(private readonly pokemonStorageService: PokemonStorageService, private readonly pokemonAPIService: PokemonApiService) {}

  /*
  * Function to dynamically fetch more pokemon on scrolling to the bottom of the page.
  * */
  @HostListener("window:scroll", [])
  public onScroll(): void
  {
    if (!this.loadingContent)
    {
      const yPos = window.scrollY + window.innerHeight;
      const bias = 100;
      if (yPos >= document.body.offsetHeight - bias)
      {
        const pokemon = this.pokemonStorageService.getPokemon();
        const offset = pokemon.length;
        const limit = 20;
        this.pokemonAPIService.fetchPokemonPage(offset, limit).subscribe({
          next: (_response) => {
            if (_response)
            {

              pokemon.push(...convertPokemonFormat(_response.results));
              this.pokemonStorageService.setPokemon(pokemon);
            }
          }
        })

      }
    }
  }

  ngOnInit(): void {}
}
