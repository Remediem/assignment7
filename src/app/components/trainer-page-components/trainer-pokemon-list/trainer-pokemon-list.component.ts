import { Component, OnInit } from '@angular/core';
import { TRAINER_STORAGE_KEY } from 'src/app/models/storage.model';
import {Trainer} from "../../../models/trainer.model";

@Component({
  selector: 'app-trainer-pokemon-list',
  templateUrl: './trainer-pokemon-list.component.html',
  styleUrls: ['./trainer-pokemon-list.component.css']
})
export class TrainerPokemonListComponent implements OnInit {

  private trainerString: string = localStorage.getItem(TRAINER_STORAGE_KEY) ?? "";
  public trainer: Trainer = JSON.parse(this.trainerString) as Trainer;
  public trainerPokemon = this.trainer.pokemon;

  constructor() { }

  ngOnInit(): void {
  }

}
