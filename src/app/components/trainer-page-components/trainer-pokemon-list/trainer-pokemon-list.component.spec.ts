import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerPokemonListComponent } from './trainer-pokemon-list.component';

describe('TrainerPokemonListComponent', () => {
  let component: TrainerPokemonListComponent;
  let fixture: ComponentFixture<TrainerPokemonListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainerPokemonListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerPokemonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
