import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { TrainerApiService } from 'src/app/services/trainer-api.service';
import { TrainerStorageService } from 'src/app/services/trainer-storage.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import capitalize from 'src/app/util/capitalize';

const { imgURL } = environment;

@Component({
  selector: 'app-trainer-pokemon',
  templateUrl: './trainer-pokemon.component.html',
  styleUrls: ['./trainer-pokemon.component.css'],
})
export class TrainerPokemonComponent implements OnInit {
  @Input() pokemon: Pokemon = {name: "", id: -1};

  public url = imgURL;

  public capitalize = (_input: string) => capitalize(_input);

  constructor(
    private readonly trainerApiService: TrainerApiService,
    private readonly trainerStorageService: TrainerStorageService
  ) {}

  /**
   * Release the pokemon from the trainer when the release button is clicked.
   */
  public onRelease = () => {
    // Get the trainer Id
    const trainer = this.trainerStorageService.getTrainer();
    if (trainer === undefined) return;

    // Get the trainer and filter out the released pokemon
    if (this.pokemon.id === undefined) return;
    trainer.pokemon = trainer.pokemon.filter((pokemon) => pokemon.id !== this.pokemon.id);
    
    // Update API
    // Make new instance of trainer
    this.trainerApiService.updateTrainer(trainer.id, trainer).subscribe({
      next: () => {
        // Update localStorage
        this.trainerStorageService.setTrainer(trainer);

        // Reload page
        location.reload(); // TODO: Necessary?
      },
    });
  }
  ngOnInit(): void {}
}
