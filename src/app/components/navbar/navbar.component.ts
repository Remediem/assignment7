import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { TrainerStorageService } from 'src/app/services/trainer-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private readonly router: Router, private readonly trainerStorageService: TrainerStorageService) { }

  get routerURL()
  {
    return this.router.url;
  }

  public onLogout(): void
  {
    this.trainerStorageService.deleteTrainer();
    this.router.navigateByUrl("/");
  }

  ngOnInit(): void {
  }

}
