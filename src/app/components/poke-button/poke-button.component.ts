import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-poke-button',
  templateUrl: './poke-button.component.html',
  styleUrls: ['./poke-button.component.css']
})
export class PokeButtonComponent implements OnInit {

  @Input() pokemonIsCaught: boolean = true;
  @Input() public parentCallback: (() => void) | undefined;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * When the Poke-button is clicked, a method is ran in the parent
   * @returns 
   */
  public onPokeballClick(){
    // Make sure everything is allright
    if (this.parentCallback === undefined) return
    
    // Activate selected
    this.pokemonIsCaught = !this.pokemonIsCaught;

    // Run parent callback
    this.parentCallback()
  }

}
