import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerStorageService } from 'src/app/services/trainer-storage.service';
import { PokemonStorageService } from 'src/app/services/pokemon-storage.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage implements OnInit {
  constructor() {}

  ngOnInit(): void {
  }
}
