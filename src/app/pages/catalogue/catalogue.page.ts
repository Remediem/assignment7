import {Component, OnInit} from '@angular/core';
import {Pokemon} from 'src/app/models/pokemon.model';
import {PokemonApiService} from 'src/app/services/pokemon-api.service';
import {PokemonStorageService} from 'src/app/services/pokemon-storage.service';
import convertPokemonFormat from 'src/app/util/convertPokemonFormat';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css'],
})
export class CataloguePage implements OnInit {

  public pokemonList : Pokemon[] = [];

  constructor(
    private readonly pokemonApiService: PokemonApiService,
    private readonly pokemonStorageService: PokemonStorageService
  ) {}

  ngOnInit(): void {
    // Fetch 20 pokemon
    this.pokemonApiService.fetchPokemonPage(0, 20).subscribe({
      next: (_response) => {
        if (_response)
        {
          this.pokemonList = convertPokemonFormat(_response.results);
        }
        this.pokemonStorageService.setPokemon(this.pokemonList);
      }
    });
  }
}
