// Contains the keys used to store Trainers and Pokemons in the Local Storage
export const TRAINER_STORAGE_KEY = "trainer";
export const POKEMON_LIST_STORAGE_KEY = "pokemon-list";
