import {Pokemon} from "./pokemon.model";

export interface TrainerResponse
{
  id: number
  username: string
  pokemon: Pokemon[]
}

export interface Trainer
{
  username: string
  pokemon: Pokemon[]
}
