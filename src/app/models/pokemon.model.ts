export interface PokemonAPIResponse
{
  results: PokemonFromAPI[]
}

/** Pokemons are returned on this format from the API */
export interface PokemonFromAPI {
  name: string,
  url: string
}

/** This is the format we want our pokmon in */
export interface Pokemon {
  name: string;
  id: number
}
