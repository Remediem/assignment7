import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import { LandingPage } from "./pages/landing/landing.page";
import { TrainerPage } from "./pages/trainer/trainer.page";
import {CataloguePage} from "./pages/catalogue/catalogue.page";
import { AuthGuard } from "./guards/auth.guard";
import { IsLoggedInGuard } from "./guards/is-logged-in.guard";

const routes: Routes = [
  {
    path: "",
    component: LandingPage,
    canActivate: [IsLoggedInGuard]
  },
  {
    path: "trainer",
    component: TrainerPage,
    canActivate: [AuthGuard]
  },
  {
    path: "catalogue",
    component: CataloguePage,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
