import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UserLoginComponent } from './components/landing-page-components/user-login/user-login.component';
import { TrainerPokemonListComponent } from './components/trainer-page-components/trainer-pokemon-list/trainer-pokemon-list.component';
import { TrainerPokemonComponent } from './components/trainer-page-components/trainer-pokemon/trainer-pokemon.component';
import { CataloguePokemonListComponent } from './components/catalogue-page-components/catalogue-pokemon-list/catalogue-pokemon-list.component';
import { CataloguePokemonComponent } from './components/catalogue-page-components/catalogue-pokemon/catalogue-pokemon.component';
import { LandingPage } from './pages/landing/landing.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { PokeButtonComponent } from './components/poke-button/poke-button.component'
import { NavbarComponent } from './components/navbar/navbar.component'

@NgModule({
  declarations: [
    AppComponent,
    UserLoginComponent,
    TrainerPokemonListComponent,
    TrainerPokemonComponent,
    CataloguePokemonListComponent,
    CataloguePokemonComponent,
    LandingPage,
    TrainerPage,
    CataloguePage,
    PokeButtonComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
