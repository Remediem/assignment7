import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { TrainerStorageService } from '../services/trainer-storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly trainerStorageService: TrainerStorageService
  ) {}

  /**
   * When Trainers are not logged in, they shuold be rerouted to to landing page at "/"
   * @param route 
   * @param state 
   * @returns 
   */
  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
  Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.trainerStorageService.getTrainer() !== undefined) {
      return true;
    }
    else {
      this.router.navigateByUrl("/"); // Go to login page if no trainer is set
      return false;
    }
  }

  

}
