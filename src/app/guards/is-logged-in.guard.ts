import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TrainerStorageService } from '../services/trainer-storage.service';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedInGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly trainerStorageService: TrainerStorageService
  ) {}
  
  /**
   * When Trainers are logged in, they should be rerouted to "/catalogue" when they try to access the login page.
   * @param route 
   * @param state 
   * @returns 
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.trainerStorageService.getTrainer() === undefined) {
      return true;
    }
    else {
      this.router.navigateByUrl("/catalogue"); // Go to catalogue if user is already logged in
      return false;
    }
  }
}
