import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Pokemon, PokemonAPIResponse, PokemonFromAPI} from "../models/pokemon.model";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

const { pokemonAPI } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {


  constructor(private readonly http: HttpClient) {}

  /*
  * Function to fetch a page of pokemon, and store it in private variable accessible by getter/setter.
  */
  public fetchPokemonPage(_offset: number, _limit: number) : Observable<PokemonAPIResponse | undefined>
  {
    return this.http.get<PokemonAPIResponse>(`${pokemonAPI}?offset=${_offset}&limit=${_limit}`);
  }

}
