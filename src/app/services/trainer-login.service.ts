import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TrainerStorageService } from './trainer-storage.service';
import { TrainerApiService } from './trainer-api.service';

@Injectable({
  providedIn: 'root',
})
export class TrainerLoginService {
  // Import methods for communicating with API and Local Storage
  constructor(
    private readonly router: Router,
    private readonly trainerAPIService: TrainerApiService,
    private readonly trainerStorageService: TrainerStorageService
  ) {}

  /**
   * Takes a trainer as an input and logs them in the API and Local Storage
   * @param trainer
   */
  public logInTrainer(trainerUsername: string): void {
    // Check if trainer exists.
    this.trainerAPIService.getTrainer(trainerUsername).subscribe({
      next: (trainerFromApi) => {
        // If trainer doesn't exist --> Create one
        if (trainerFromApi === undefined) {
          this.trainerAPIService.createTrainer(trainerUsername).subscribe({
            next: (trainerResponse) => {
              // Store trainer to LocalStorage
              if (trainerResponse !== undefined) {
                this.trainerStorageService.setTrainer(trainerResponse);
                // Redirect
                this.router.navigateByUrl('/catalogue');
              }
            },
          });
        } else {
          // Store trainer to LocalStorage
          this.trainerStorageService.setTrainer(trainerFromApi);
          // Redirect
          this.router.navigateByUrl('/catalogue');
        }
      },
    });
  }
}
