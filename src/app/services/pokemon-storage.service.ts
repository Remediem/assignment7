import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { POKEMON_LIST_STORAGE_KEY } from '../models/storage.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonStorageService {


  constructor() {}

  /**
   * Fetch the pokemon currently stored in Local Storage
   * @returns 
   */
  public getPokemon(): Pokemon[]
  {
    const pokemonData = localStorage.getItem(POKEMON_LIST_STORAGE_KEY);
    if (!pokemonData) return []

    const storedPokemon = JSON.parse(pokemonData) as Pokemon[];
    return storedPokemon;
  }

  /**
   * Store a list of Pokemon in Local Storage
   */
  public setPokemon(pokemonList: Pokemon[]):void {
    localStorage.setItem(POKEMON_LIST_STORAGE_KEY, JSON.stringify(pokemonList))
  }

  
}
