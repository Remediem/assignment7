import {Injectable} from '@angular/core';
import {TRAINER_STORAGE_KEY} from '../models/storage.model';
import {TrainerResponse} from '../models/trainer.model';

@Injectable({
  providedIn: 'root',
})
export class TrainerStorageService {

  /**
   * Fetch the current trainer stored in Local Storage
   * @returns
   */
  public getTrainer()
  {
    const trainerData = localStorage.getItem(TRAINER_STORAGE_KEY);
    if (!trainerData) return undefined
    return JSON.parse(trainerData) as TrainerResponse;
  }

  /**
   * Store the current trainer to localStorage.
   * @param _trainer
   */
  public setTrainer(_trainer: TrainerResponse | undefined)
  {
    localStorage.setItem(TRAINER_STORAGE_KEY, JSON.stringify(_trainer));
  }

  /**
   * Remove the current trainer from localStorage.
   * @param _trainer
   */
  public deleteTrainer()
  {
    localStorage.removeItem(TRAINER_STORAGE_KEY);
  }

  constructor() {}
}
