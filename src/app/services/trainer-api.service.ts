import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, Observable } from 'rxjs';
import { Trainer, TrainerResponse } from '../models/trainer.model';

const { trainerAPI, trainerAPIKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class TrainerApiService {
  constructor(private readonly http: HttpClient) {}

  /*
   * Function to create trainer in trainer api, by username
   */
  public createTrainer(
    _username: string
  ): Observable<TrainerResponse | undefined> {
    const trainer = {
      username: _username,
      pokemon: [],
    };

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': trainerAPIKey,
    });

    return this.http.post<TrainerResponse>(`${trainerAPI}/trainers`, trainer, {
      headers,
    });
  }

  /*
   * Function to get trainer from trainer api, by username
   * */
  public getTrainer(
    _username: string
  ): Observable<TrainerResponse | undefined> {
    return this.http
      .get<TrainerResponse[]>(`${trainerAPI}/trainers?username=${_username}`)
      .pipe(
        map((_trainers: TrainerResponse[]) => {
          return _trainers.pop();
        })
      );
  }

  /*
   * Function to update trainer of specific id with new values.
   */
  public updateTrainer(
    _id: Number,
    _trainer: Trainer | TrainerResponse
  ): Observable<TrainerResponse | undefined> {
    const trainer: Trainer = {
      username: _trainer.username,
      pokemon: _trainer.pokemon,
    };

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': trainerAPIKey,
    });

    return this.http.patch<TrainerResponse>(
      `${trainerAPI}/trainers/${_id}`,
      trainer,
      { headers }
    );
  }

  /*
   * Function to delete trainer from trainer api, by id.
   */
  public deleteTrainer(_id: number): Observable<TrainerResponse | undefined> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': trainerAPIKey,
    });

    return this.http
      .delete<TrainerResponse[]>(`${trainerAPI}/trainers/${_id}`, { headers })
      .pipe(
        map((_trainers: TrainerResponse[]) => {
          return _trainers.pop();
        })
      );
  }
}
