/**
 * We want all pokemon-names capitalized
 * @param pokemonName
 * @returns
 */
const capitalize = (pokemonName: string) => {
  return pokemonName.charAt(0).toUpperCase() + pokemonName.slice(1);
};

export default capitalize;
