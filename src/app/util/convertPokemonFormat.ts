import { Pokemon, PokemonFromAPI } from "../models/pokemon.model";


/**
   * Convert an array of PokemonFromAPI to an array of Pokemon
   * @param pokemonsFromAPi 
   * @returns 
   */
 const convertPokemonFormat = (pokemonsFromAPi: PokemonFromAPI[]): Pokemon[] => {
  const pokemons : Pokemon[] = []
  pokemonsFromAPi.forEach( pokemon => {
    // Identify ID
    const url = pokemon.url;
    const strs = url.split("/");
    const id = strs[strs.length-2]

    // Put into Pokemon format
    const newPokemon: Pokemon = {
      name: pokemon.name,
      id: parseInt(id)
    }
    // Put each Pokemon into 
    pokemons.push(newPokemon)
  } )
  return pokemons
}

export default convertPokemonFormat;