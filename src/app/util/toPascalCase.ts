/**
   * We want all pokemon-names capitalized
   * @param pokemonName
   * @returns
   */
 const capitalizePokemonName(pokemonName: string) => {
  return pokemonName.charAt(0).toUpperCase() + pokemonName.slice(1);
}

export default capitalizePokemonName