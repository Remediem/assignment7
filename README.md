# Pokemon Trainer App using Angular
Attempt to create a pokemon trainer app using Angular.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.5.

## Background
Noroff assignment

## Getting started
Clone the project.
cd into project.
run `npm install` to install all modules.
run `ng serve` and go to the host url.

## Usage
Go to the host url. Provide a username.
Click on the pokeball, and you will be taken to the catalogue page. Catch and release whichever pokemon you want. All your caught pokemon can be found by navigating to the trainer page, in the nav bar. 

You can also visit a hosted version of this project on:
`https://radiant-ocean-50263.herokuapp.com/`.

## Authors
Sigurd Skaga, Amund Kulsrud Storruste
